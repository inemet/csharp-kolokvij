﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_kolokvij {
    sealed class Predmet {
        private Predmet predmet;

        public Predmet(String naziv, String odredište, DateTime isporučeno, decimal težina, bool opasno) {
            Naziv = naziv;
            Odredište = odredište;
            Isporučeno = isporučeno;
            Težina = težina;
            Opasno = opasno;
        }

        internal bool toString() {
            throw new NotImplementedException();
        }

        public Predmet(Predmet predmet) {
            this.predmet = predmet;
        }

        public override string ToString() {
            return String.Format("{0}/{1}/{2}", Naziv, Odredište, Opasno);
        }


        public String Naziv { get; set; }
        public String Odredište { get; set; }
        public DateTime Isporučeno { get; set; }
        public decimal Težina { get; set; }
        public bool Opasno { get; set; }

    }
}
