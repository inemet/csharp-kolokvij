﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_kolokvij {
    abstract class Skladiste {
        public Skladiste(int brojPredmeta, bool oprema) {
            BrojPredmeta = brojPredmeta;
            Oprema = oprema;
        }

        public int BrojPredmeta { get; set; }
        public bool Oprema { get; set; }

        public abstract void DodajPredmet(string Naziv, string Odredište, DateTime Isporučeno, decimal Težina, bool Opasno);

        /*public int brojPredmeta { 
            get {
                return brojPredmeta;
            }
            set {
                if (value > brojPredmeta)
                    value = brojPredmeta;
                } else {
                throw new Exception("Unesite veći broj");
            }
            }*/


        public virtual void PrikaziPredmete() {
            foreach (var predmet in predmeti) {
                Console.WriteLine(predmet.ToString());
            }
        }


        protected List<Predmet> predmeti;
    }
}
